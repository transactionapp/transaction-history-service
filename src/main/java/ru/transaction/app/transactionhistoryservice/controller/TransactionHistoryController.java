package ru.transaction.app.transactionhistoryservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.transaction.app.transactionhistoryservice.mapper.TransactionHistoryMapper;
import ru.transaction.app.transactionhistoryservice.model.dto.TransactionHistoryDto;
import ru.transaction.app.transactionhistoryservice.service.TransactionHistoryService;

import java.util.List;

@RestController
@RequestMapping("v1/transactions-history")
public class TransactionHistoryController {

    @Autowired
    private TransactionHistoryMapper mapper;

    @Autowired
    private TransactionHistoryService service;

    @GetMapping
    public List<TransactionHistoryDto> get(
            @RequestParam(value = "userId") String userId,
            @RequestParam(value = "offset", defaultValue = "0") int offset,
                                           @RequestParam(value = "limit", defaultValue = "1000") int limit) {
        return mapper.toDtos(service .get(userId, offset, limit));
    }

}
