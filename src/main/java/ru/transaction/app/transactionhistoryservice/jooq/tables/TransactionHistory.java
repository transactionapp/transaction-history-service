/*
 * This file is generated by jOOQ.
 */
package ru.transaction.app.transactionhistoryservice.jooq.tables;


import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Function8;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Records;
import org.jooq.Row8;
import org.jooq.Schema;
import org.jooq.SelectField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;

import ru.transaction.app.transactionhistoryservice.jooq.Indexes;
import ru.transaction.app.transactionhistoryservice.jooq.Keys;
import ru.transaction.app.transactionhistoryservice.jooq.TransactionHistoryServiceSchema;
import ru.transaction.app.transactionhistoryservice.jooq.tables.records.TransactionHistoryRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TransactionHistory extends TableImpl<TransactionHistoryRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of
     * <code>transaction_history_service_schema.transaction_history</code>
     */
    public static final TransactionHistory TRANSACTION_HISTORY = new TransactionHistory();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TransactionHistoryRecord> getRecordType() {
        return TransactionHistoryRecord.class;
    }

    /**
     * The column
     * <code>transaction_history_service_schema.transaction_history.id</code>.
     */
    public final TableField<TransactionHistoryRecord, UUID> ID = createField(DSL.name("id"), SQLDataType.UUID.nullable(false).defaultValue(DSL.field(DSL.raw("gen_random_uuid()"), SQLDataType.UUID)), this, "");

    /**
     * The column
     * <code>transaction_history_service_schema.transaction_history.transaction_id</code>.
     */
    public final TableField<TransactionHistoryRecord, String> TRANSACTION_ID = createField(DSL.name("transaction_id"), SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column
     * <code>transaction_history_service_schema.transaction_history.attempts</code>.
     */
    public final TableField<TransactionHistoryRecord, Integer> ATTEMPTS = createField(DSL.name("attempts"), SQLDataType.INTEGER.defaultValue(DSL.field(DSL.raw("0"), SQLDataType.INTEGER)), this, "");

    /**
     * The column
     * <code>transaction_history_service_schema.transaction_history.state</code>.
     */
    public final TableField<TransactionHistoryRecord, String> STATE = createField(DSL.name("state"), SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column
     * <code>transaction_history_service_schema.transaction_history.author_id</code>.
     */
    public final TableField<TransactionHistoryRecord, String> AUTHOR_ID = createField(DSL.name("author_id"), SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column
     * <code>transaction_history_service_schema.transaction_history.recipient_id</code>.
     */
    public final TableField<TransactionHistoryRecord, String> RECIPIENT_ID = createField(DSL.name("recipient_id"), SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column
     * <code>transaction_history_service_schema.transaction_history.transaction_created_timestamp</code>.
     */
    public final TableField<TransactionHistoryRecord, OffsetDateTime> TRANSACTION_CREATED_TIMESTAMP = createField(DSL.name("transaction_created_timestamp"), SQLDataType.TIMESTAMPWITHTIMEZONE(6), this, "");

    /**
     * The column
     * <code>transaction_history_service_schema.transaction_history.created_timestamp</code>.
     */
    public final TableField<TransactionHistoryRecord, OffsetDateTime> CREATED_TIMESTAMP = createField(DSL.name("created_timestamp"), SQLDataType.TIMESTAMPWITHTIMEZONE(6), this, "");

    private TransactionHistory(Name alias, Table<TransactionHistoryRecord> aliased) {
        this(alias, aliased, null);
    }

    private TransactionHistory(Name alias, Table<TransactionHistoryRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased
     * <code>transaction_history_service_schema.transaction_history</code> table
     * reference
     */
    public TransactionHistory(String alias) {
        this(DSL.name(alias), TRANSACTION_HISTORY);
    }

    /**
     * Create an aliased
     * <code>transaction_history_service_schema.transaction_history</code> table
     * reference
     */
    public TransactionHistory(Name alias) {
        this(alias, TRANSACTION_HISTORY);
    }

    /**
     * Create a
     * <code>transaction_history_service_schema.transaction_history</code> table
     * reference
     */
    public TransactionHistory() {
        this(DSL.name("transaction_history"), null);
    }

    public <O extends Record> TransactionHistory(Table<O> child, ForeignKey<O, TransactionHistoryRecord> key) {
        super(child, key, TRANSACTION_HISTORY);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : TransactionHistoryServiceSchema.TRANSACTION_HISTORY_SERVICE_SCHEMA;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.asList(Indexes.THS_TRANSACTION_HISTORY_AUTHOR_ID, Indexes.THS_TRANSACTION_HISTORY_RECIPIENT_ID);
    }

    @Override
    public UniqueKey<TransactionHistoryRecord> getPrimaryKey() {
        return Keys.TRANSACTION_HISTORY_PKEY;
    }

    @Override
    public TransactionHistory as(String alias) {
        return new TransactionHistory(DSL.name(alias), this);
    }

    @Override
    public TransactionHistory as(Name alias) {
        return new TransactionHistory(alias, this);
    }

    @Override
    public TransactionHistory as(Table<?> alias) {
        return new TransactionHistory(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public TransactionHistory rename(String name) {
        return new TransactionHistory(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TransactionHistory rename(Name name) {
        return new TransactionHistory(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public TransactionHistory rename(Table<?> name) {
        return new TransactionHistory(name.getQualifiedName(), null);
    }

    // -------------------------------------------------------------------------
    // Row8 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row8<UUID, String, Integer, String, String, String, OffsetDateTime, OffsetDateTime> fieldsRow() {
        return (Row8) super.fieldsRow();
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Function)}.
     */
    public <U> SelectField<U> mapping(Function8<? super UUID, ? super String, ? super Integer, ? super String, ? super String, ? super String, ? super OffsetDateTime, ? super OffsetDateTime, ? extends U> from) {
        return convertFrom(Records.mapping(from));
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Class,
     * Function)}.
     */
    public <U> SelectField<U> mapping(Class<U> toType, Function8<? super UUID, ? super String, ? super Integer, ? super String, ? super String, ? super String, ? super OffsetDateTime, ? super OffsetDateTime, ? extends U> from) {
        return convertFrom(toType, Records.mapping(from));
    }
}
