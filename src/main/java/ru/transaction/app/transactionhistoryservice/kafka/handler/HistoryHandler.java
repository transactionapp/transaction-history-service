package ru.transaction.app.transactionhistoryservice.kafka.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.transaction.app.transactionhistoryservice.jooq.tables.pojos.TransactionHistory;
import ru.transaction.app.transactionhistoryservice.mapper.TransactionPaymentMapper;
import ru.transaction.app.transactionhistoryservice.model.kafka.TransactionPaymentDto;
import ru.transaction.app.transactionhistoryservice.model.type.TransactionState;
import ru.transaction.app.transactionhistoryservice.service.TransactionHistoryService;

@Component
public class HistoryHandler {

    @Autowired
    private TransactionPaymentMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TransactionHistoryService transactionHistoryService;

    @KafkaListener(topics = "transaction_app_payment_success_topic")
    public void handle(ConsumerRecord<String, String> record) throws JsonProcessingException {
        String key = record.key();
        String value = record.value();
        TransactionPaymentDto dto = objectMapper.readValue(value, TransactionPaymentDto.class);
        TransactionHistory transactionHistory = mapper.toPojo(dto);
        transactionHistory.setAttempts(Integer.valueOf(key.split("_")[1]));
        transactionHistory.setState(TransactionState.SUCCESS.name());
        transactionHistoryService.save(transactionHistory);
    }

    @KafkaListener(topics = "transaction_app_payment_dead_letter_topic")
    public void handleDeadLetter(ConsumerRecord<String, String> record) throws JsonProcessingException {
        String key = record.key();
        String value = record.value();
        TransactionPaymentDto dto = objectMapper.readValue(value, TransactionPaymentDto.class);
        TransactionHistory transactionHistory = mapper.toPojo(dto);
        transactionHistory.setAttempts(Integer.valueOf(key.split("_")[1]));
        transactionHistory.setState(TransactionState.FAIL.name());
        transactionHistoryService.save(transactionHistory);
    }

}
