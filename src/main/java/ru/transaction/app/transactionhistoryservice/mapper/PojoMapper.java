package ru.transaction.app.transactionhistoryservice.mapper;

import java.util.Collection;
import java.util.List;

public interface PojoMapper<P, D> {

    P toPojo(D dto);

    D toDto(P pojo);

    List<P> toPojos(Collection<D> dtos);

    List<D> toDtos(Collection<P> pojos);
}
