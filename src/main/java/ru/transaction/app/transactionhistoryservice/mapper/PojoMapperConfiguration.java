package ru.transaction.app.transactionhistoryservice.mapper;

import org.mapstruct.MapperConfig;
import org.mapstruct.MappingConstants;

@MapperConfig(componentModel = MappingConstants.ComponentModel.SPRING)
public interface PojoMapperConfiguration {
}
