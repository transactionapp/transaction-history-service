package ru.transaction.app.transactionhistoryservice.mapper;

import org.mapstruct.Mapper;
import ru.transaction.app.transactionhistoryservice.jooq.tables.pojos.TransactionHistory;
import ru.transaction.app.transactionhistoryservice.model.dto.TransactionHistoryDto;

@Mapper(config = PojoMapperConfiguration.class)
public abstract class TransactionHistoryMapper implements PojoMapper<TransactionHistory, TransactionHistoryDto> {
}
