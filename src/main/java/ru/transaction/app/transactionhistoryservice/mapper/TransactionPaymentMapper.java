package ru.transaction.app.transactionhistoryservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import ru.transaction.app.transactionhistoryservice.jooq.tables.pojos.TransactionHistory;
import ru.transaction.app.transactionhistoryservice.model.kafka.TransactionPaymentDto;

@Mapper(config = PojoMapperConfiguration.class)
public abstract class TransactionPaymentMapper implements PojoMapper<TransactionHistory, TransactionPaymentDto> {


    @Override
    @Mappings({
            @Mapping(source = "id", target = "transactionId"),
            @Mapping(source = "createdTimestamp", target = "transactionCreatedTimestamp"),
            @Mapping(target = "createdTimestamp", ignore = true)
    })
    public abstract TransactionHistory toPojo(TransactionPaymentDto dto);


    public  TransactionPaymentDto toDto(TransactionHistory pojo) {
        throw new UnsupportedOperationException();
    }
}
