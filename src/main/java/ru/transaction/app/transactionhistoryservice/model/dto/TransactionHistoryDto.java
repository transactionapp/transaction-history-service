package ru.transaction.app.transactionhistoryservice.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionHistoryDto {

    private UUID id;
    private String transactionId;
    private Integer attempts;
    private String state;
    private String authorId;
    private String recipientId;
    private OffsetDateTime transactionCreatedTimestamp;
    private OffsetDateTime createdTimestamp;

}
