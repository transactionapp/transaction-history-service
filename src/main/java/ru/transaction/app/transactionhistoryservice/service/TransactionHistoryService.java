package ru.transaction.app.transactionhistoryservice.service;


import ru.transaction.app.transactionhistoryservice.jooq.tables.pojos.TransactionHistory;

import java.util.List;

public interface TransactionHistoryService {

    void save(TransactionHistory transactionHistory);

    List<TransactionHistory> get(String userId, int offset, int limit);

}
