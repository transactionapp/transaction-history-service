package ru.transaction.app.transactionhistoryservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.transaction.app.transactionhistoryservice.jooq.tables.daos.TransactionHistoryDao;
import ru.transaction.app.transactionhistoryservice.jooq.tables.pojos.TransactionHistory;
import ru.transaction.app.transactionhistoryservice.service.TransactionHistoryService;

import java.time.OffsetDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionHistoryServiceImpl implements TransactionHistoryService {

    private final TransactionHistoryDao dao;

    @Override
    public void save(TransactionHistory transactionHistory) {
        transactionHistory.setCreatedTimestamp(OffsetDateTime.now());
        dao.insert(transactionHistory);
    }

    @Override
    public List<TransactionHistory> get(String userId, int offset, int limit) {
        return dao.ctx()
                .selectFrom(ru.transaction.app.transactionhistoryservice.jooq.tables.TransactionHistory.TRANSACTION_HISTORY)
                .where(ru.transaction.app.transactionhistoryservice.jooq.tables.TransactionHistory.TRANSACTION_HISTORY.AUTHOR_ID.eq(userId))
                .fetchInto(TransactionHistory.class);
    }
}
