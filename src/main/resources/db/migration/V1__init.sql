create schema if not exists transaction_history_service_schema;

create table if not exists transaction_history_service_schema.transaction_history
(
    id                            uuid not null default gen_random_uuid() primary key,
    transaction_id                text not null,
    attempts                      int           default 0,
    state                         text not null,
    author_id                     text not null,
    recipient_id                  text not null,
    transaction_created_timestamp timestamp with time zone,
    created_timestamp             timestamp with time zone
);

create index if not exists ths_transaction_history_author_id on transaction_history_service_schema.transaction_history (author_id);
create index if not exists ths_transaction_history_recipient_id on transaction_history_service_schema.transaction_history (recipient_id);